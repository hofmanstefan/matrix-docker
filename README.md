# Matrix Docker

## Generate Synapse config

`docker-compose run --rm -e SYNAPSE_SERVER_NAME=my.matrix.host -e SYNAPSE_REPORT_STATS=no synapse generate`

[More](https://github.com/matrix-org/synapse/tree/develop/contrib/docker) info can be found here.

## Mautrix-WhatsApp

https://github.com/tulir/mautrix-whatsapp/wiki/Bridge-setup-with-Docker

## Start Synapse

`docker-compose up -d`
